![vatapp logo](logo.png)

Before charging VAT for service and goods in Poland, there is a legal obligation to confirm whether a customer is an active VAT Taxpayer. This law passed in 2017. Each verification should be saved as a proof.

Ministry of Finance opened **VAT Service** to verify the validity of VAT numbers - it's a not user-friendly website: [https://ppuslugi.mf.gov.pl/?link=VAT](https://ppuslugi.mf.gov.pl/?link=VAT)  
There is no REST API, SOAP web service, anything useful to programmatically automate this process ...  
The only way to get Proof of Verification is to click through the web form and make a `print/print to pdf` of a website.  
This print works as a valid legal proof. There is not even a simple unique verification ID. (sic!)  
`(╯°□°)╯︵ ⊥ᴎƎWᴎᴚƎᴧO⅁`

## `vatapp` simplify and allows to automate VAT number verification providing REST API and fast async implementation in Node.js

#### implementation

`vatapp` implements **VAT Service** by web scraping and adding a custom API gate for [https://ppuslugi.mf.gov.pl/?link=VAT](https://ppuslugi.mf.gov.pl/?link=VAT)

## Install with npm or yarn

```bash
$ git clone https://gitlab.com/marcinlerka/vatapp.git
$ cd vatapp

// npm
$ npm install

// yarn
$ yarn
```

Run locally  

```bash
// npm
$ npm start

// yarn
$ yarn start
```
Head out to [http://localhost:3000](http://localhost:3000)

## Build with Docker

Build
```bash
$ docker build -t vatapp .
```

Run locally and expose port 3000
```bash
$ docker run --name vatapp -d -p 3000:3000 vatapp
```

### Usage + sample requests

```bash
$ curl http://localhost:3000/vatpl/8960006282

// response:
{"vatNumber":"8960006282","vatStatus":"Podmiot o podanym identyfikatorze podatkowym NIP jest zarejestrowany jako podatnik VAT czynny"}
```

```bash
$ curl http://localhost:3000/vatpl/6921055536

// response:
{"vatNumber":"6921055536","vatStatus":"Podmiot o podanym identyfikatorze podatkowym NIP jest zarejestrowany jako podatnik VAT zwolniony"}
```

```bash
$ curl http://localhost:3000/vatpl/8961000821

// response:
{"vatNumber":"8961000821","vatStatus":"Podmiot o podanym identyfikatorze podatkowym NIP nie jest zarejestrowany jako podatnik VAT"}
```

## To Do

- [ ] pdf/png screenshot [verification proof]
- [ ] add EU Vat verification service (VIES)
- [x] Docker
- [ ] cli
- [ ] auth + rate limit
- [ ] browser frontend
