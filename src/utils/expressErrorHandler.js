'use strict'

const logger = require('../log.js')
const serverErrorLog = (message) => {
  logger.log('error', message)
}
const clientErrorLog = (message) => {
  logger.log('warn', message)
}

module.exports = (err, req, res, next) => {
  const clientErrors = {
    400: '400 Bad Request',
    401: '401 Unauthorized',
    404: '404 Not Found',
    422: '422 Unprocessable Entity'
  }

  const gatewayErrors = {
    502: '502 Bad Gateway'
  }

  const serverError = () => {
    serverErrorLog({
      status: 500,
      method: req.method,
      request: req.originalUrl,
      message: JSON.stringify(err.message) || err.toString(),
      stack: JSON.stringify(err.stack) || err.toString()
    })
    res.status(500).send({ error: '500 Internal Server Error' })
  }

  const gatewayError = () => {
    let error = {
      status: err.status,
      message: clientErrors[err.status],
      details: err.errors
    }

    clientErrorLog({
      status: err.status,
      method: req.method,
      request: req.originalUrl,
      message: gatewayErrors[err.status],
      details: err.errors
    })
    res.status(err.status).send(error)
  }

  const clientError = () => {
    let error = {
      status: err.status,
      message: clientErrors[err.status],
      details: err.errors
    }

    clientErrorLog({
      status: err.status,
      method: req.method,
      request: req.originalUrl,
      message: clientErrors[err.status],
      details: err.errors
    })
    res.status(err.status).send(error)
  }

  clientErrors[err.status]
    ? clientError()
    : gatewayErrors[err.status]
      ? gatewayError()
      : serverError()
}
