'use strict'

const logger = require('../log.js')
const log = msg => logger.log('info', msg)

exports.index = (req, res) => {
  res.status(200).send({
    message: '200 GET request OK'
  })
  log({
    status: 200,
    method: req.method,
    request: req.originalUrl
  })
}
