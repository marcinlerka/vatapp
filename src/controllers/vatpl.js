'use strict'

const plVatService = require('../services/plVatService.js')
const plVatFormat = require('../services/plVatFormat.js')
const logger = require('../log.js')
const log = msg => logger.log('info', msg)

exports.vatpl = (req, res, next) => {
  req.checkParams('vatNumber', 'Vat number should be 10 digit number or number with dashes').matches(/^([\d-]{10,13}$)/i) // regexp refactor needed: add dots, improve digits length validation
  const validationErrors = req.validationErrors()

  if (validationErrors) {
    return next({
      status: 400,
      errors: validationErrors
    })
  }

  const plVatServiceError = (err) => {
    const errorType = err.message
    const errorMessage = plVatService.errorMessages['PL'][errorType]

    return errorType === 'OTHER'
      ? next({status: 502, errors: errorMessage})
      : next({status: 422, errors: errorMessage})
  }

  const serverError = (err) => {
    return next(err)
  }

  plVatFormat.formatVatNumber(req.params.vatNumber)
    .then(formattedVatNumber => {
      return plVatService.checkVat(formattedVatNumber.vatNumber)
    })
    .then(checked => {
      log({
        status: 200,
        method: req.method,
        request: req.originalUrl,
        vatStatus: checked.response
      })
      return res.json({
        vatNumber: checked.vatNumber,
        vatStatus: plVatService.statusMessages['PL'][checked.response]
      })
    })
    .catch(error => {
      return plVatService.errorMessages['PL'][error.message]
        ? plVatServiceError(error)
        : serverError(error)
    })
}
