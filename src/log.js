'use strict'

const dotenv = require('dotenv')
dotenv.load({ path: '.env.default' })
const winston = require('winston')
winston.emitErrs = true

const consoleTransport = new winston.transports.Console({
  handleExceptions: true,
  colorize: true
  // json: true,
  // timestamp: true
})

const fileTransport = new winston.transports.File({
  filename: `./logs/${process.env.NODE_ENV}.log`,
  handleExceptions: true,
  json: true,
  timestamp: true
})

const logger = new winston.Logger({
  transports: [
    ...(process.env.NODE_ENV === 'development' ? [consoleTransport] : []),
    fileTransport
  ],
  exitOnError: false
})

module.exports = logger
