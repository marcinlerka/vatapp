'use strict'

const express = require('express')
const expressValidator = require('express-validator')
const helmet = require('helmet')
const errorHandler = require('./utils/expressErrorHandler.js')
const app = express()

const homeController = require('./controllers/home')
const vatplController = require('./controllers/vatpl')

app.use(helmet())
app.use(expressValidator())

app.get('/', homeController.index)
app.get('/vatpl/:vatNumber', vatplController.vatpl)

app.use((req, res, next) => {
  return next({status: 404})
})
app.use(errorHandler)

module.exports = app
