/* eslint-disable no-unused-vars */
'use strict'

const dotenv = require('dotenv')
const app = require('./app.js')
dotenv.load({ path: '.env.default' })

app.set('host', process.env.HOST || '0.0.0.0')
app.set('port', process.env.PORT || 3000)

const server = app.listen(app.get('port'), app.get('host'), () => {
  console.info(`> 🌎 Server is listening on http://${app.get('host')}:${app.get('port')}/`)
})
