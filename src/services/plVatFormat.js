const formatVatNumber = (vatNumber) => {
  return Promise.resolve()
    .then(() => {
      const cleansedVatPl = vatNumber.replace(/[\s.\-,]/g, '') // cleanse = remove whitespaces, dots, hyphens/dashes
      const formattedVatPl = {
        countryCode: 'PL',
        vatNumber: cleansedVatPl
      }
      const formatError = () => { throw new Error('INVALID_FORMAT_INTERNAL_CHECK') }

      return cleansedVatPl.match(/^(\d{10}$)/i)
        ? formattedVatPl
        : formatError()
    })
    .catch(error => {
      throw error
    })
}

module.exports = { formatVatNumber }
