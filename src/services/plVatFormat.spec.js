/* eslint-env mocha */
'use strict'

const chai = require('chai')
const chaiAsPromised = require('chai-as-promised')
const expect = chai.expect
chai.use(chaiAsPromised)

const plVatFormat = require('./plVatFormat.js')

describe('PL Vat number formatting function', () => {
  it('should remove whitespaces from Vat number and return without errors if Vat number has 10 digits', () => {
    let whitespaces = '8 5510 25  211'
    let whitespacesResult = {
      countryCode: 'PL',
      vatNumber: '8551025211'
    }

    let promise = plVatFormat.formatVatNumber(whitespaces)
    return expect(promise).to.eventually.deep.equal(whitespacesResult)
  })

  it('should remove dots from Vat number and return without errors if Vat number has 10 digits', () => {
    let dots = '8.55.10.25.21 1'
    let dotsResult = {
      countryCode: 'PL',
      vatNumber: '8551025211'
    }

    let promise = plVatFormat.formatVatNumber(dots)
    return expect(promise).to.eventually.deep.equal(dotsResult)
  })

  it('should remove hyphens/dashes from Vat number and return without errors if Vat number has 10 digits', () => {
    let hyphens = '8-5-510-25  -211'
    let hyphensResult = {
      countryCode: 'PL',
      vatNumber: '8551025211'
    }

    let promise = plVatFormat.formatVatNumber(hyphens)
    return expect(promise).to.eventually.deep.equal(hyphensResult)
  })

  it('should throw invalid format internal check error for Vat number longer than 10 digits', () => {
    let vatNumber = '85510252111111'
    let promise = plVatFormat.formatVatNumber(vatNumber)
    return expect(promise).to.be.rejectedWith('INVALID_FORMAT_INTERNAL_CHECK')
  })

  it('should throw invalid format internal check error for Vat number containing non digit characters', () => {
    let vatNumber = '8abc025298'
    let promise = plVatFormat.formatVatNumber(vatNumber)
    return expect(promise).to.be.rejectedWith('INVALID_FORMAT_INTERNAL_CHECK')
  })
})
