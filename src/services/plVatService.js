'use strict'

const cheerio = require('cheerio')
let request = require('request-promise')

const checkVat = (vatNumber) => {
  const cookies = request.jar()

  const init = () => {
    return request({
      uri: 'https://ppuslugi.mf.gov.pl/_/',
      resolveWithFullResponse: true,
      jar: cookies
    })
  }

  const action = (token1st) => {
    return request({
      method: 'POST',
      uri: 'https://ppuslugi.mf.gov.pl/_/ExecuteAction',
      form: {
        'ACTION__': '1005',
        'FAST_VERLAST__': token1st
      },
      resolveWithFullResponse: true,
      jar: cookies
    })
  }

  const verification = (token2nd) => {
    return request({
      method: 'POST',
      uri: 'https://ppuslugi.mf.gov.pl/_/EventOccurred',
      form: {
        'b-6': 'NIP',
        'b-7': vatNumber,
        'DOC_MODAL_ID__': '0',
        'EVENT__': 'b-8',
        'FAST_VERLAST__': token2nd
      },
      json: true,
      jar: cookies
    })
  }

  return Promise.resolve()
    .then(init)
    .then(response => {
      let token1st = response.headers['fast-ver-last']
      return token1st
    })
    .then(action)
    .then(response => {
      let token2nd = response.headers['fast-ver-last']
      return token2nd
    })
    .then(verification)
    .then(response => {
      if (response.includes('Niepoprawny Numer Identyfikacji Podatkowej')) {
        throw new Error('INVALID_NUMBER')
      } else if (response.includes('Niepoprawny format')) {
        throw new Error('INVALID_FORMAT')
      }

      let $ = cheerio.load(response)
      let htmlContent = $.html()
      htmlContent = htmlContent.replace(/\\&quot;/g, '')
      $ = cheerio.load(htmlContent)
      let statusHtml = $('#caption2_b-3').html()

      const htmlResponseError = () => {
        throw new Error('HTML_RESPONSE_ERROR')
      }

      const vatStatus = () => {
        return statusHtml.includes('NIP jest zarejestrowany jako podatnik VAT czynny')
          ? 'REGISTERED'
          : statusHtml.includes('NIP nie jest zarejestrowany jako podatnik VAT')
            ? 'NOT_REGISTERED'
            : statusHtml.includes('NIP jest zarejestrowany jako podatnik VAT zwolniony')
              ? 'EXEMPT'
              : htmlResponseError()
      }

      let result = {
        vatNumber: vatNumber,
        response: vatStatus()
      }
      return result
    })
    .catch(error => {
      throw error
    })
}

const errorMessages = {
  'PL': {
    'INVALID_NUMBER': 'Niepoprawny Numer Identyfikacji Podatkowej - format:9999999999.',
    'INVALID_FORMAT': 'Niepoprawny format. Format:9999999999.',
    'INVALID_FORMAT_INTERNAL_CHECK': 'Niepoprawny format NIP PL kontrachenta.',
    'OTHER': 'Nieznany błąd serwisu ppuslugi.mf.gov.pl',
    'HTML_RESPONSE_ERROR': 'Nieznany błąd serwisu ppuslugi.mf.gov.pl'
  }
}

const statusMessages = {
  'PL': {
    'REGISTERED': 'Podmiot o podanym identyfikatorze podatkowym NIP jest zarejestrowany jako podatnik VAT czynny',
    'NOT_REGISTERED': 'Podmiot o podanym identyfikatorze podatkowym NIP nie jest zarejestrowany jako podatnik VAT',
    'EXEMPT': 'Podmiot o podanym identyfikatorze podatkowym NIP jest zarejestrowany jako podatnik VAT zwolniony'
  }
}

module.exports = { checkVat, errorMessages, statusMessages }
