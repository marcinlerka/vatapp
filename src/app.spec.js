/* eslint-env mocha */
'use strict'

const supertest = require('supertest')
const chai = require('chai')
const chaiAsPromised = require('chai-as-promised')
chai.use(chaiAsPromised)
const app = require('./app.js')
let server
let request

before(done => {
  console.log('---> NOCK is OFF. Real HTTP calls <---') // refactor: add http mocks like in plVatService.spec

  server = app.listen(done)
  request = supertest.agent(server)
})

after(done => {
  server.close(done)
})

describe('GET /', () => {
  it('should return 200 OK', () => {
    return request
      .get('/')
      .expect(200)
  })
})

describe('GET /vatpl/:vatNumber', () => {
  it('/vatpl/8960006282 should respond with registered message for registered vat number [200 OK]', () => {
    return request
      .get('/vatpl/8960006282')
      .expect(200)
      .expect({
        vatNumber: '8960006282',
        vatStatus: 'Podmiot o podanym identyfikatorze podatkowym NIP jest zarejestrowany jako podatnik VAT czynny'
      })
  })

  it('/vatpl/8961000821 should respond with not registered message for not registered vat number [200 OK]', () => {
    return request
      .get('/vatpl/8961000821')
      .expect(200)
      .expect({ vatNumber: '8961000821',
        vatStatus: 'Podmiot o podanym identyfikatorze podatkowym NIP nie jest zarejestrowany jako podatnik VAT'
      })
  })

  it('/vatpl/123-45-67-890 should return [422 Unprocessable Entity]', () => {
    return request
      .get('/vatpl/123-45-67-890')
      .expect(422)
      .expect({
        status: 422,
        message: '422 Unprocessable Entity',
        details: 'Niepoprawny Numer Identyfikacji Podatkowej - format:9999999999.'
      })
  })

  it('/vatpl/1234567890 should return [422 Unprocessable Entity]', () => {
    return request
      .get('/vatpl/1234567890')
      .expect(422)
      .expect({
        status: 422,
        message: '422 Unprocessable Entity',
        details: 'Niepoprawny Numer Identyfikacji Podatkowej - format:9999999999.'
      })
  })

  it('/vatpl/1234567.890 should return [400 Bad Request]', () => {
    return request
      .get('/vatpl/1234567.890')
      .expect(400)
      .expect({
        status: 400,
        message: '400 Bad Request',
        'details': [
          {
            msg: 'Vat number should be 10 digit number or number with dashes',
            location: 'params',
            param: 'vatNumber',
            value: '1234567.890'
          }
        ]
      })
  })

  it('/vatpl/1234567 should return [400 Bad Request]', () => {
    return request
      .get('/vatpl/1234567')
      .expect(400)
      .expect({
        status: 400,
        message: '400 Bad Request',
        'details': [
          {
            msg: 'Vat number should be 10 digit number or number with dashes',
            location: 'params',
            param: 'vatNumber',
            value: '1234567'
          }
        ]
      })
  })

  it('/vatpl/12345asdfg should return [400 Bad Request]', () => {
    return request
      .get('/vatpl/12345asdfg')
      .expect(400)
      .expect({
        status: 400,
        message: '400 Bad Request',
        details: [
          {
            msg: 'Vat number should be 10 digit number or number with dashes',
            location: 'params',
            param: 'vatNumber',
            value: '12345asdfg'
          }
        ]
      })
  })
})

describe('GET /random_url', () => {
  it('should return 404 Not found', () => {
    return request
      .get('/random_url')
      .expect(404)
  })
})
