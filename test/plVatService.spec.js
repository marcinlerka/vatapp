/* eslint-env mocha */
'use strict'

const chai = require('chai')
const chaiAsPromised = require('chai-as-promised')
const expect = chai.expect
chai.use(chaiAsPromised)

const plVatService = require('../src/services/plVatService.js')

describe('PL Vat Service', () => {
  if (process.env.NOCK_OFF !== 'true') {
    console.log('---> NOCK is ON. Mocking HTTP calls <---')
    require('./mock/plVatService.js')
  } else {
    console.log('---> NOCK is OFF. Real HTTP calls <---')
  }

  it('should respond with registered message for registered vat number', () => {
    let nip = '8960006282'
    let expectedResult = {
      vatNumber: '8960006282',
      response: 'REGISTERED'
    }

    let promise = plVatService.checkVat(nip)

    return Promise.all([
      expect(promise).not.to.be.rejected,
      expect(promise).to.eventually.have.all.keys('vatNumber', 'response'),
      expect(promise).to.eventually.deep.equal(expectedResult)
    ])
  })

  it('should respond with not registered message for not registered vat number', () => {
    let nip = '8961000821'
    let expectedResult = {
      vatNumber: '8961000821',
      response: 'NOT_REGISTERED'
    }

    let promise = plVatService.checkVat(nip)

    return Promise.all([
      expect(promise).not.to.be.rejected,
      expect(promise).to.eventually.have.all.keys('vatNumber', 'response'),
      expect(promise).to.eventually.deep.equal(expectedResult)
    ])
  })

  it('should respond with exempt message for exempt vat number', () => {
    let nip = '6921055536'
    let expectedResult = {
      vatNumber: '6921055536',
      response: 'EXEMPT'
    }

    let promise = plVatService.checkVat(nip)

    return Promise.all([
      expect(promise).not.to.be.rejected,
      expect(promise).to.eventually.have.all.keys('vatNumber', 'response'),
      expect(promise).to.eventually.deep.equal(expectedResult)
    ])
  })

  it('should throw invalid number error for invalid vat number', () => {
    let nip = '8551025298'
    let promise = plVatService.checkVat(nip)
    return expect(promise).to.be.rejectedWith('INVALID_NUMBER')
  })

  it('should throw invalid number error for invalid vat number with more than 10 chars', () => {
    let nip = '85510252981111'
    let promise = plVatService.checkVat(nip)
    return expect(promise).to.be.rejectedWith('INVALID_NUMBER')
  })

  it('should throw invalid format error for vat number with letters', () => {
    let nip = '85510252pl'
    let promise = plVatService.checkVat(nip)
    return expect(promise).to.be.rejectedWith('INVALID_FORMAT')
  })
})
