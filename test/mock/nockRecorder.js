const nock = require('nock')
const fs = require('fs')

const plVatService = require('../../src/services/plVatService.js')

const appendLogToFile = (content) => {
  fs.appendFile('./test/mock/plVatService.js', content, (error) => {
    if (error) throw error
    console.log('done')
  })
}

nock.recorder.rec({
  logging: appendLogToFile,
  dont_print: false,
  output_objects: false,
  use_separator: false
})

const vatNumbers = [
  '8960006282', // registered
  '8961000821', // not registered
  '6921055536', // exempt
  '8551025298', // INVALID NUMBER
  '85510252981111', // INVALID NUMBER
  '85510252pl' // INVALID_FORMAT
]

vatNumbers.map(number => {
  console.log(`number: ${number}`)
  plVatService.checkVat(number)
    .then(result => {
      console.log('success')
      console.log(result)
      return result
    })
    .catch(error => {
      console.log(`error! number: ${number}`)
      console.log(error)
      return error
    })
})
