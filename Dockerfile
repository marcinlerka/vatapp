FROM node:8.9

RUN mkdir -p /src

COPY . /src

WORKDIR /src

RUN yarn

ENV PORT 3000
ENV NODE_ENV development
EXPOSE $PORT

CMD ["yarn", "start"]
